# Formulaire d'inscription en PHP

Ce projet est un simple formulaire d'inscription qui sauvegarde les informations saisies dans un fichier. Il utilise HTML, CSS, JavaScript et PHP. Les données du formulaire sont sauvegardées dans trois formats différents : txt, csv et json.

# Structure du Projet

<!-- 
# - `/txt_output` : Dossier où les données du formulaire sont enregistrées au format .txt
# - `/csv_output` : Dossier où les données du formulaire sont enregistrées au format .csv
# - `/json_output` : Dossier où les données du formulaire sont enregistrées au format .json
 -->

## Pré-requis

1. XAMPP (qui inclut PHP et Apache)
2. Un navigateur web

## Installation

1. Clonez ce dépôt dans le dossier htdocs de votre installation XAMPP.


    ```bash
    git clone https://gitlab.com/Sahraoui59/exo_formulaire_afpa.git
    ```

2. Assurez-vous que XAMPP est en fonctionnement et configuré pour traiter les fichiers PHP.

## Utilisation

1. Ouvrez votre navigateur web et naviguez jusqu'à l'emplacement du projet. Par exemple, si vous utilisez un serveur local, l'adresse pourrait être `http://localhost/formulaire-inscription`.

2. Remplissez le formulaire et cliquez sur "Soumettre".

3. Les données du formulaire seront enregistrées dans un fichier. Les différentes entrées seront séparées par des "---------------------------".

4. Pour voir les entrées enregistrées, naviguez jusqu'à `http://localhost/WorkSpace/Exo_Formulaire_Yassen/Exo_Formulaire_Json/traitement.php`.

## Contribution

Les contributions à ce projet sont les bienvenues. Pour contribuer :

1. Clonez ce dépôt et créez une nouvelle branche.

    ```bash
    git clone https://gitlab.com/Sahraoui59/exo_formulaire_afpa.git
    git checkout -b feature_branch
    ```

2. Faites vos changements et committez-les.

    ```bash
    git commit -am 'Ajouter une nouvelle fonctionnalité'
    ```

3. Poussez vos changements sur GitHub.

    ```bash
    git push origin feature_branch
    ```

4. Créez une Pull Request sur GitHub expliquant vos changements.

## Licence

Ce projet est sous licence MIT - voir le fichier [LICENSE.md](LICENSE.md) pour plus de détails.

## Contact

Pour toute question ou discussion à propos de ce projet, veuillez me contacter :

Email: your-email@example.com
