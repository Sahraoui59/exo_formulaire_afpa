<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <title>AFFICHAGE</title>
    <link rel="stylesheet" href="style.css">
  </head>
  
  <body>   
  <?php
  if ($_SERVER["REQUEST_METHOD"] === "POST") {
      // Récupération des valeurs des champs du formulaire
      $nom = $_POST['nom'];
      $prenom = $_POST['prenom'];
      $adresse = $_POST['adresse'];
      $ville = $_POST['ville'];
      $code_postal = $_POST['code_postal'];
      $telephone = $_POST['telephone'];
      $email = $_POST['email'];

      // Vérification si le fichier de sauvegarde existe
      $fichierExiste = file_exists("enregistrements.json");

      // Ouverture du fichier de sauvegarde en mode ajout ou création
      $fichier = fopen("enregistrements.json", "a");

      // Si le fichier n'existait pas, on écrit le tableau JSON vide
      if (!$fichierExiste) {
          fwrite($fichier, "[]");
      }

      // Lecture du contenu du fichier JSON
      $contenu = file_get_contents("enregistrements.json");
      $donnees = json_decode($contenu, true);

      // Création du nouvel enregistrement
      $nouvelEnregistrement = [
          "Date et heure" => date("Y-m-d H:i:s"),
          "Nom" => $nom,
          "Prénom" => $prenom,
          "Adresse" => $adresse,
          "Ville" => $ville,
          "Code Postal" => $code_postal,
          "Téléphone" => $telephone,
          "Email" => $email
      ];

      // Ajout du nouvel enregistrement dans le tableau
      $donnees[] = $nouvelEnregistrement;

      // Écriture du tableau mis à jour dans le fichier JSON
      file_put_contents("enregistrements.json", json_encode($donnees, JSON_PRETTY_PRINT));

      // Fermeture du fichier
      fclose($fichier);

      // Affichage des données
      echo "<h1>Données enregistrées :</h1>";
      echo "<table>";
      echo "<tr><th>Date et heure</th><th>Nom</th><th>Prénom</th><th>Adresse</th><th>Ville</th><th>Code Postal</th><th>Téléphone</th><th>Email</th></tr>";
      echo "<tr><td>".$nouvelEnregistrement["Date et heure"]."</td><td>".$nouvelEnregistrement["Nom"]."</td><td>".$nouvelEnregistrement["Prénom"]."</td><td>".$nouvelEnregistrement["Adresse"]."</td><td>".$nouvelEnregistrement["Ville"]."</td><td>".$nouvelEnregistrement["Code Postal"]."</td><td>".$nouvelEnregistrement["Téléphone"]."</td><td>".$nouvelEnregistrement["Email"]."</td></tr>";
      echo "</table>";

      // Affichage des données du fichier enregistrements.json
      echo "<h1>Données du fichier enregistrements.json :</h1>";
      echo "<table>";
      echo "<tr><th>Date et heure</th><th>Nom</th><th>Prénom</th><th>Adresse</th><th>Ville</th><th>Code Postal</th><th>Téléphone</th><th>Email</th></tr>";

      // Lecture du contenu du fichier "enregistrements.json"
      $contenu = file_get_contents("enregistrements.json");
      $donnees = json_decode($contenu, true);

      foreach ($donnees as $enregistrement) {
          echo "<tr><td>".$enregistrement["Date et heure"]."</td><td>".$enregistrement["Nom"]."</td><td>".$enregistrement["Prénom"]."</td><td>".$enregistrement["Adresse"]."</td><td>".$enregistrement["Ville"]."</td><td>".$enregistrement["Code Postal"]."</td><td>".$enregistrement["Téléphone"]."</td><td>".$enregistrement["Email"]."</td></tr>";
      }

      echo "</table>";
  }
  ?>
  </body>
</html>
