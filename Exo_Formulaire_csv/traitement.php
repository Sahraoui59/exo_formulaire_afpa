<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <title>AFFICHAGE</title>
    <link rel="stylesheet" href="style.css">
  </head>
  
  <body>   
  <?php
if ($_SERVER["REQUEST_METHOD"] === "POST") {
    // Récupération des valeurs des champs du formulaire
    $nom = $_POST['nom'];
    $prenom = $_POST['prenom'];
    $adresse = $_POST['adresse'];
    $ville = $_POST['ville'];
    $code_postal = $_POST['code_postal'];
    $telephone = $_POST['telephone'];
    $email = $_POST['email'];

    // Vérification si le fichier de sauvegarde existe
    $fichierExiste = file_exists("enregistrements.csv");

    // Ouverture du fichier de sauvegarde en mode ajout ou création
    $fichier = fopen("enregistrements.csv", "a");

    // Si le fichier n'existait pas, on écrit l'en-tête
    if (!$fichierExiste) {
        fputcsv($fichier, ['Date et heure', 'Nom', 'Prénom', 'Adresse', 'Ville', 'Code Postal', 'Téléphone', 'Email'], ';');
    }

    // Création d'un tableau avec les données à enregistrer
    $donnees = [
        date("Y-m-d H:i:s"),
        $nom,
        $prenom,
        $adresse,
        $ville,
        $code_postal,
        $telephone,
        $email
    ];

    // Écriture des données dans le fichier CSV
    fputcsv($fichier, $donnees, ';');

    // Fermeture du fichier
    fclose($fichier);

    // Affichage des données
    echo "<h1>Données enregistrées :</h1>";
    echo "<table>";
    echo "<tr><th>Date et heure</th><th>Nom</th><th>Prénom</th><th>Adresse</th><th>Ville</th><th>Code Postal</th><th>Téléphone</th><th>Email</th></tr>";
    echo "<tr><td>". $donnees[0] ."</td><td>". $donnees[1] ."</td><td>". $donnees[2] ."</td><td>". $donnees[3] ."</td><td>". $donnees[4] ."</td><td>". $donnees[5] ."</td><td>". $donnees[6] ."</td><td>". $donnees[7] ."</td></tr>";
    echo "</table>";

    // Affichage des données du fichier enregistrements.csv
    echo "<h1>Données du fichier enregistrements.csv :</h1>";
    echo "<table>";
    echo "<tr><th>Date et heure</th><th>Nom</th><th>Prénom</th><th>Adresse</th><th>Ville</th><th>Code Postal</th><th>Téléphone</th><th>Email</th></tr>";

    // Lecture du contenu du fichier "enregistrements.csv"
    if (($fichier = fopen("enregistrements.csv", "r")) !== FALSE) {
        while (($donnees = fgetcsv($fichier, 1000, ";")) !== FALSE) {
            if (count($donnees) >= 8) {
                echo "<tr><td>". $donnees[0] ."</td><td>". $donnees[1] ."</td><td>". $donnees[2] ."</td><td>". $donnees[3] ."</td><td>". $donnees[4] ."</td><td>". $donnees[5] ."</td><td>". $donnees[6] ."</td><td>". $donnees[7] ."</td></tr>"; } } fclose($fichier); } echo "</table>"; } ?>
</body>
</html>
        